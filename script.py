#############
#@AUTHOR: JAKUB WOŁODKO(FIZO)
#@DECIMAL GEOGRAPHICAL COORDINATES CONVERTER
############

line = []
sepLine = []
decimalVal = []
DATA_FILE = 'dane.txt'
afterConvert = []
with open(DATA_FILE) as f:
    lines = f.readlines()
    line = lines

def truncate(n, decimals=0):
    multiplier = 10 ** decimals
    return int(n * multiplier) / multiplier

def formatData():
    for v in sepLine:
        for index in range(0,4):
            data = v[index]
            deg = data[2:4]
            minutes = data[5:7]
            seconds = data[8:15]
            decimalGeo = truncate(int(deg)+(int(minutes)/60)+(float(seconds)/3600),5)
            decimalVal.append(decimalGeo)
    createDataFile()

def createDataFile():
    pomocnicza = 0
    evenData = decimalVal[::2]
    oddData = decimalVal[1::2]
    for v in range(0,len(oddData)):
        if pomocnicza >= 2:
            afterConvert.append('-1\n')
            pomocnicza = 0
            afterConvert.append(str(evenData[v])+"+"+str(oddData[v])+"\n")
            print (pomocnicza)
        else:
            afterConvert.append(str(evenData[v])+"+"+str(oddData[v])+"\n")
            print (pomocnicza)
        pomocnicza = pomocnicza + 1
    createFile()

def createFile():
    f = open("final.txt", "a")
    f.writelines(afterConvert)
    print("DATA SAVE")
    f.close()
def execConvertToDEC():
    if len(line) > 0:
        for x in line:
            sepLine.append(x.split())
        formatData()
execConvertToDEC()
